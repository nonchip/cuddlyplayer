lgi = require 'lgi'
GLib = lgi.GLib
Gst = lgi.Gst

class GstTagGetter
  cb: (bus,message)=>
    if message.type.STATE_CHANGED
      old, new, pending = message\parse_state_changed!
      if old == 'READY' and new == 'PAUSED' and pending == 'VOID_PENDING' and @_gotSome
        @pipeline.state='NULL'
        @playbin.state = 'NULL'
        @._cb @_uri, @_tags
        @_tags=nil
        @_uri=nil
        @_cb=nil
        @_gotSome=false
        @running=false
        @try_next_task!
    elseif message.type.TAG
      message\parse_tag!\foreach (list,tag)->
        @_tags[tag] = tostring list\get tag
        @_gotSome=true
    elseif message.type.EOS or message.type.ERROR
      @pipeline.state='NULL'
      @playbin.state = 'NULL'
      @_gotSome=false
      @_tags=nil
      @_uri=nil
      @_cb=nil
      @running=false
    return true
  new: =>
    @tasks={}
    @running=false
    @pipeline = Gst.Pipeline!
    @playbin = Gst.ElementFactory.make 'playbin', nil
    @pipeline\add @playbin
    @pipeline.bus\add_watch GLib.PRIORITY_DEFAULT, @\cb
  __gc: => @destroy!
  destroy: =>
    @pipeline.state = 'NULL'
    @playbin.state = 'NULL'
  for_uri: (uri,cb)=>
    @tasks[#(@tasks)+1] = {uri,cb}
    @try_next_task!
  try_next_task: =>
    if not @running and #(@tasks)>0
      @handle_task table.remove @tasks, 1
  handle_task: (task)=>
    @running=true
    @_cb=task[2]
    @_uri=task[1]
    @_tags={}
    @_gotSome=false
    @playbin.uri = task[1]
    @pipeline.state = 'PAUSED'
    @playbin.state = 'PAUSED'
