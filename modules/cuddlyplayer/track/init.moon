lgi = require 'lgi'
GLib = lgi.GLib
GstTagGetter = require 'cuddlyplayer.track.gst_tag_getter'

resolve_static_tag_getter= do
  local static_tag_getter
  (using static_tag_getter)->
    if not static_tag_getter
      print 'Track: initializing static tag_getter'
      static_tag_getter = GstTagGetter!
    static_tag_getter


class Track extends require 'cuddlyplayer.track.gvariantable_interface'
  new: (uri_or_variant,@logstatus=print,@addcb=nil,@tag_getter=nil)=>
    if type(uri_or_variant) == 'string'
      @uri = uri_or_variant
      if not @tag_getter
        @tag_getter = resolve_static_tag_getter!
      @tags={}
      @tag_getter\for_uri @uri, @\tag_getter_cb
    elseif uri_or_variant.type == 'a{sv}'
      variant = uri_or_variant.value
      @uri = variant.uri
      @tags = {k,v for k,v in GLib.Variant.lookup_value(uri_or_variant,'tags',GLib.VariantType.new'a{ss}')\pairs!}
    else
      error "Track: TYPEWTF: "..type(uri_or_variant)
  __tostring: =>
    if @tags.artist and @tags.title
      @tags.artist .. ': ' .. @tags.title
    else
      @uri
  tag_getter_cb: (uri,tags)=>
    if uri ~= @uri
      @.logstatus 'Track', 'uri mismatch in tag_getter_cb'
      print uri, '~=', @uri
      return
    @tags = tags
    if @addcb
      @addcb!
  to_variant: =>
    GLib.Variant 'a{sv}', {
      uri: GLib.Variant 's', @uri
      tags: GLib.Variant 'a{ss}', @tags
    }
