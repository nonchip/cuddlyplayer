import dump from require 'moon'

gsplit = (sep, plain)=>
    @=tostring @
    start = 1
    done = false
    pass=(i, j, ...)->
      if i
        seg = @\sub(start, i - 1)
        start = j + 1
        return seg, ...
      else
        done = true
        return @\sub(start)
    return ->
      return if done
      if sep == ''
        done = true
        return @
      return pass @\find sep, start, plain
indent = (str,by)-> table.concat [by..line for line in gsplit str,'\n',true when #line>0], '\n'

class Interface
  _interface_handle_illegal_access: (name)=>
    (...)->
      error 'ILLEGAL INTERFACE ACCESS: called %s\\%s(\n%s\n)'\format (@__class.__name or '[anon class]'), name, indent dump(...),'  '
  new: (...)=>
    mt = getmetatable @
    old_index = mt.__index
    mt.__index = (name) =>
      if type(old_index) == "function"
        if old = old_index @, name
          return old
        if hc = old_index @, "_interface_handle_illegal_access"
          return hc @, name
      else
        if old = old_index[name]
          return old
        if hc = old_index._interface_handle_illegal_access
          return hc @, name
    if hc = @._interface_handle_illegal_access
      hc(@, 'new') ...
