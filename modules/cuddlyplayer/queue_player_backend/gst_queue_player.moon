lgi = require 'lgi'
GLib = lgi.GLib
Gst = lgi.Gst

class GstQueuePlayer extends require 'cuddlyplayer.queue_player_backend.interface'
  new: (@logstatus=print,@statecb=nil,@queue=nil)=>
    @player = Gst.ElementFactory.make 'playbin', nil
    @player.bus\add_watch GLib.PRIORITY_DEFAULT, @\cb
  __gc: => @destroy!
  destroy: =>
    @player.state = 'NULL'
  cb: (bus,message)=>
    if message.type.ERROR
      @.logstatus 'QueuePlayer', 'Error: ' .. message\parse_error!.message
      @player.state = 'NULL'
      if @statecb
        @.statecb false
    elseif message.type.EOS
      @switch_to_next!
    return true
  switch_to_next: =>
    return nil, 'no Queue' unless @queue
    if @statecb
      @.statecb false
    @player.state = 'NULL'
    @track=@queue\next_track!
    if @track
      @.logstatus 'QueuePlayer', 'Playing:'..tostring(@track)
      @player.uri=@track.uri
      @player.state='PLAYING'
      if @statecb
        @.statecb true, @track
  pause: =>
    @player.state = 'PAUSED'
    @was_paused = true
    if @statecb
      @.statecb false, '[PAUSED] '..tostring(@track)
  play: =>
    if @was_paused
      @player.state = 'PLAYING'
      if @statecb
        @.statecb true, @track
    else
      @switch_to_next!
