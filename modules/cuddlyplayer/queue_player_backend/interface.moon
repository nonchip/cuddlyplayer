class QueuePlayerInterface extends require 'cuddlyplayer.interface'
  new: (@logstatus=print,@statecb=nil,@queue=nil)=> super @logstatus, @statecb, @queue
  __gc: => @destroy!
  destroy: => super!
  switch_to_next: => super!
  pause: => super!
  play: => super!
