lgi = require 'lgi'
GLib = lgi.GLib
Gtk = lgi.Gtk
GObject = lgi.GObject

Track = require 'cuddlyplayer.track'

class Queue
  --columns: {
    --{ 'TAG', 'artist', 'Artist', GObject.Type.STRING }
    --{ 'TAG', 'title',  'Title',  GObject.Type.STRING }
    --{ 'SYS', 'uri',    'URI',    GObject.Type.STRING }
  --}
  new: (@logstatus=print)=>
    --@colnames={c[2],i for i,c in ipairs @columns}
    --[c[4] for c in *@columns]
    @ls = Gtk.ListStore.new { GObject.Type.VARIANT }
  add_track: (track)=>
    @ls\append {track\to_variant!}
    @.logstatus 'Queue', 'Added: ' .. tostring(track)
  next_track: =>
    iter = @ls\get_iter_first!
    return nil unless iter
    variant = @ls[iter][1]
    return nil unless variant
    track = Track variant
    @ls\remove iter
    return track
