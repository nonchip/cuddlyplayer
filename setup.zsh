#!/bin/zsh

export CUDDLYPLAYER_PATH="$(dirname $(readlink -f $0))"
export CUDDLYPLAYER_REAL_ROOT="$CUDDLYPLAYER_PATH/.root"
export CUDDLYPLAYER_SRC="$CUDDLYPLAYER_PATH/.src"
export CUDDLYPLAYER_ROOT="/tmp/.cuddlyplayer.$(uuidgen -t)-$(uuidgen -r)"

continue_stage=n
if [ -f "$CUDDLYPLAYER_PATH/.continue_stage" ]
  then continue_stage=$(cat "$CUDDLYPLAYER_PATH/.continue_stage")
fi

if [ -f "$CUDDLYPLAYER_PATH/.continue_root" ]
  then CUDDLYPLAYER_ROOT=$(cat "$CUDDLYPLAYER_PATH/.continue_root")
fi

case $continue_stage in
  n)
    rm -f "$CUDDLYPLAYER_PATH/.continue_stage"
    rm -rf "$CUDDLYPLAYER_ROOT" "$CUDDLYPLAYER_SRC" "$CUDDLYPLAYER_REAL_ROOT"
    mkdir -p "$CUDDLYPLAYER_REAL_ROOT" "$CUDDLYPLAYER_SRC"
    ln -s "$CUDDLYPLAYER_REAL_ROOT" "$CUDDLYPLAYER_ROOT"
    echo "$CUDDLYPLAYER_ROOT" > "$CUDDLYPLAYER_PATH/.continue_root"
    ;&
  luajit) v=8271c643c21d1b2f344e339f559f2de6f3663191
    echo "luajit" > "$CUDDLYPLAYER_PATH/.continue_stage"
    cd $CUDDLYPLAYER_SRC
    git clone http://luajit.org/git/luajit-2.0.git luajit || exit
    cd luajit
    git checkout ${v}
    make amalg PREFIX=$CUDDLYPLAYER_ROOT CPATH=$CUDDLYPLAYER_ROOT/include LIBRARY_PATH=$CUDDLYPLAYER_ROOT/lib CFLAGS='-DLUAJIT_ENABLE_LUA52COMPAT -DLUAJIT_ENABLE_GC64' && \
    make install PREFIX=$CUDDLYPLAYER_ROOT || exit
    ln -sf $(find $CUDDLYPLAYER_ROOT/bin/ -name "luajit-2.1*" | head -n 1) $CUDDLYPLAYER_ROOT/bin/luajit
    ;&
  luarocks) v=d2718bf39dace0af009b9484fc6019b276906023
    echo "luarocks" > "$CUDDLYPLAYER_PATH/.continue_stage"
    cd $CUDDLYPLAYER_SRC
    git clone https://github.com/luarocks/luarocks.git || exit
    cd luarocks
    git checkout ${v}
    git pull
    ./configure --prefix=$CUDDLYPLAYER_ROOT \
                --lua-version=5.1 \
                --lua-suffix=jit \
                --with-lua=$CUDDLYPLAYER_ROOT \
                --with-lua-include=$CUDDLYPLAYER_ROOT/include/luajit-2.1 \
                --with-lua-lib=$CUDDLYPLAYER_ROOT/lib/lua/5.1 \
                --force-config && \
    make build && make install || exit
    ;&
  moonscript)
    echo "moonscript" > "$CUDDLYPLAYER_PATH/.continue_stage"
    $CUDDLYPLAYER_ROOT/bin/luarocks install moonscript || exit
    ;&
  lgi)
    echo "lgi" > "$CUDDLYPLAYER_PATH/.continue_stage"
    $CUDDLYPLAYER_ROOT/bin/luarocks install lgi || exit
    ;&
  loadkit)
    echo "loadkit" > "$CUDDLYPLAYER_PATH/.continue_stage"
    $CUDDLYPLAYER_ROOT/bin/luarocks install loadkit || exit
    ;&
  luafilesystem)
    echo "luafilesystem" > "$CUDDLYPLAYER_PATH/.continue_stage"
    $CUDDLYPLAYER_ROOT/bin/luarocks install luafilesystem || exit
    ;&
  wrappers)
    echo "wrappers" > "$CUDDLYPLAYER_PATH/.continue_stage"
    # wrappers
    cat > $CUDDLYPLAYER_PATH/.run <<END
#!/bin/zsh
export CUDDLYPLAYER_PATH="\$(dirname "\$(readlink -f "\$0")")"
export CUDDLYPLAYER_REAL_ROOT="\$CUDDLYPLAYER_PATH/.root"
export CUDDLYPLAYER_ROOT="$CUDDLYPLAYER_ROOT"

[ -e "\$CUDDLYPLAYER_ROOT" ] || ln -s "\$CUDDLYPLAYER_PATH/.root" \$CUDDLYPLAYER_ROOT

export PATH="\$CUDDLYPLAYER_ROOT/bin:\$CUDDLYPLAYER_ROOT/nginx/sbin:\$PATH"
export LD_LIBRARY_PATH="\$CUDDLYPLAYER_ROOT/lib:\$LD_LIBRARY_PATH"

path_prefixes=(./custom_ \$CUDDLYPLAYER_PATH/ \$CUDDLYPLAYER_PATH/modules/ \$CUDDLYPLAYER_ROOT/lualib/ \$CUDDLYPLAYER_ROOT/share/luajit-2.1.0-beta3/ \$CUDDLYPLAYER_ROOT/share/lua/5.1/)

LUA_PATH=""
LUA_CPATH=""
MOON_PATH=""

for prefix (\$path_prefixes)
  do LUA_PATH="\$LUA_PATH;\${prefix}?.lua;\${prefix}?/init.lua"
  LUA_CPATH="\$LUA_CPATH;\${prefix}?.so;"
  MOON_PATH="\$MOON_PATH;\${prefix}?.moon;\${prefix}?/init.moon"
done

export LUA_PATH
export LUA_CPATH
export MOON_PATH

fn=\$(basename \$0)
if [ "\$fn" = ".run" ]
  then exec "\$@"
else
  exec \$fn "\$@"
fi
END
    chmod a+rx $CUDDLYPLAYER_PATH/.run
    ;&
  moonc_all)
    echo "moonc_all" > "$CUDDLYPLAYER_PATH/.continue_stage"
    $CUDDLYPLAYER_PATH/.run moonc $CUDDLYPLAYER_PATH || exit
    ;&
esac

# cleanup
rm -rf "$CUDDLYPLAYER_SRC"
rm -f "$CUDDLYPLAYER_ROOT" "$CUDDLYPLAYER_PATH/.continue_stage" "$CUDDLYPLAYER_PATH/.continue_root"
