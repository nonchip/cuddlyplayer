_FILE_ARGS_ = {arg[0], ...}
loadkit = require 'loadkit'
lgi = require 'lgi'

GLib = lgi.GLib
Gtk = lgi.Gtk
Gst = lgi.Gst
Gio = lgi.Gio

loadkit.register 'ui', (file)->
  builder=Gtk.Builder!
  builder\add_from_string file\read '*a'
  builder.objects

Queue = require 'cuddlyplayer.queue.gtk_liststore_queue'
QueuePlayer = require 'cuddlyplayer.queue_player_backend.gst_queue_player'
Track = require 'cuddlyplayer.track'

print 'using gst: %s'\format Gst._version

class MainApplication
  logstatus: (domain,msg)=>
    print domain,msg
    id = @UI.statusbar\get_context_id domain
    @UI.statusbar\push id, msg
  statuscb: (@playing,track)=>
    if @UI
      @UI.mainwin.title = 'CuddlyPlayer' .. (track and (': ' .. tostring(track)) or '')
      @UI.playpausebutton.stock_id = 'gtk-media-' .. (@playing and 'pause' or 'play')
  new: =>
    @queue = Queue @\logstatus
    @player = QueuePlayer @\logstatus, @\statuscb, @queue
    @GAPP = Gtk.Application.new 'de.cuddlyrobot.cuddlyplayer', Gio.ApplicationFlags.HANDLES_OPEN
    do
      this = @
      with @GAPP
        .on_activate = (using this)=>
          this\load_ui!
          this.UI.mainwin\show_all!
        .on_open = (files using this, Track)=>
          this\load_ui!
          this.UI.mainwin\show_all!
          for i,file in ipairs files
            track=Track file\get_uri!, this\logstatus, (using this, i)=>
              this.queue\add_track @
              if i==1
                this.player\play!

  load_ui: =>
    @UI = require 'mainwin'
    do
      this = @
      with @UI.mainwin
        .application = this.GAPP
        .on_delete_event = (using this)=>
          this.player\destroy!
          @destroy!
          this.GAPP\quit!
      @UI.nextbutton.on_clicked = (using this)=>
        this.player\switch_to_next!
      @UI.playpausebutton.on_clicked = (using this)=>
        if this.playing
          this.player\pause!
        else
          this.player\play!
      with @UI.queueview
        .model = @queue.ls
        for i,c in ipairs{{'Artist','tags','artist'}, {'Title','tags','title'}, {'URI','uri'}}
          \append_column Gtk.TreeViewColumn {
            title: c[1]
            sort_column_id: i-1
            {
              Gtk.CellRendererText {}
              (col,cell,model,iter,udata)->
                out=Track model[iter][1]
                for i=2,#c
                  break unless out
                  out=out[c[i]]
                cell.text = out or '[UNK]'
            }
          }
      --g_signal_connect( G_OBJECT((*this_).use_db_file_chooser), "delete-event", G_CALLBACK(gtk_widget_hide_on_delete), NULL );
      @UI.addtrackdialog.on_delete_event = Gtk.Widget.hide_on_delete
      with @UI.addbutton
        .on_clicked = (using this,Track)=>
          atd = this.UI.addtrackdialog
          ret = atd\run!
          atd\hide!
          if ret == 1
            for uri in *(atd\get_uris!)
              Track uri, this\logstatus, this.queue\add_track
          d = atd\get_current_folder_uri!
          if d
            atd\set_current_folder_uri d

  run: (args=_FILE_ARGS_)=>
    @GAPP\run args

app = MainApplication!
app\run!


--with QueuePlayer GML
--  .queue\add_track 'file:///run/user/1000/gvfs/sftp:host=10.0.0.5,user=root/volumes/4tb2/Music/farnsworth-text.mp3'
--  .queue\add_track 'file:///run/user/1000/gvfs/sftp:host=10.0.0.5,user=root/volumes/4tb2/Music/Böhse Onkelz/20 Jahre/20 Jahre (Gestern)/07 - Böhse Onkelz - Terpentin.mp3'
--  \play!
